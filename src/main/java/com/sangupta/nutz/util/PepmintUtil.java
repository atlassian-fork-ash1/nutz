package com.sangupta.nutz.util;

import com.sangupta.pepmint.Formatter;
import com.sangupta.pepmint.Lexer;
import com.sangupta.pepmint.Pepmint;

public class PepmintUtil {

    public static String highlight(String formattedCode, String language1) {
        Pepmint pepmint = new Pepmint();
        Lexer lexer = pepmint.newLexer(language1);
        Formatter formatter = pepmint.newHtmlFormatter("");
        return pepmint.highlight(formattedCode, lexer, formatter);
    }

}
